package tn.esprit.spring.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import tn.esprit.spring.entities.Departement;
import tn.esprit.spring.entities.Employe;
import tn.esprit.spring.entities.Entreprise;
import tn.esprit.spring.repository.DepartementRepository;
import tn.esprit.spring.repository.EntrepriseRepository;

@Service
public class EntrepriseServiceImpl implements IEntrepriseService {

	@Autowired
    EntrepriseRepository entrepriseRepoistory;
	@Autowired
	DepartementRepository deptRepoistory;
	
	public int ajouterEntreprise(Entreprise entreprise) {
		entrepriseRepoistory.save(entreprise);
		return entreprise.getId();
	}

	
	
	public void affecterDepartementAEntreprise(int depId, int entrepriseId) {
		//Le bout Master de cette relation N:1 est departement  
				//donc il faut rajouter l'entreprise a departement 
				// ==> c'est l'objet departement(le master) qui va mettre a jour l'association
				//Rappel : la classe qui contient mappedBy represente le bout Slave
				//Rappel : Dans une relation oneToMany le mappedBy doit etre du cote one.
				Entreprise entrepriseManagedEntity;
				Optional<Entreprise> optEntreprise = entrepriseRepoistory.findById(entrepriseId);
				
				if(optEntreprise.isPresent()) {
					entrepriseManagedEntity= optEntreprise.get();
					
					Departement depManagedEntity;
					Optional<Departement> optDepartement = deptRepoistory.findById(depId);
					if(optDepartement.isPresent()) {
						depManagedEntity= optDepartement.get();
						
						depManagedEntity.setEntreprise(entrepriseManagedEntity);
						deptRepoistory.save(depManagedEntity);
				
					}
				}

				
				
	}
	


	
	public List<String> getAllDepartementsNamesByEntreprise(int entrepriseId) {
		Entreprise entrepriseManagedEntity ;
		Optional<Entreprise> opttEntreprise = entrepriseRepoistory.findById(entrepriseId);
		List<String> depNames = new ArrayList<>();

		if(opttEntreprise.isPresent()) {
			
			entrepriseManagedEntity = opttEntreprise.get();
			
		}
		
		
		return depNames;
	}

	@Transactional
	public void deleteEntrepriseById(int entrepriseId) {
		Optional<Entreprise> optEntreprise = entrepriseRepoistory.findById(entrepriseId);
if(optEntreprise.isPresent()) {
			
	     entrepriseRepoistory.delete(optEntreprise.get());			
		}
	}

	

	public Entreprise getEntrepriseById(int entrepriseId) {
		Optional<Entreprise> optEntreprise = entrepriseRepoistory.findById(entrepriseId);
		Entreprise entreprise ;
		if(optEntreprise.isPresent()) {
			entreprise = optEntreprise.get();
			return entreprise;

			}else {
				return null;
			}

	}
	
	public List<Employe> getAllEmployeByEntreprise(Entreprise entreprise) {
		return entrepriseRepoistory.getAllEmployeByEntreprisec(entreprise);
	}
	

}
